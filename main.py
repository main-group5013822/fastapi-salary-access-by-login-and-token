from fastapi import FastAPI, HTTPException
from datetime import datetime, timedelta
import secrets

app = FastAPI()


# Имитация базы данных
db = {
    'user_1': {
        'пароль': 'pass123',
        'имя': 'Bob',
        'текущая_зарплата': '$1400',
        'следующее_повышение': '01.08.2023'
    },
    'user_2': {
        'пароль': 'pass321',
        'имя': 'John',
        'текущая_зарплата': '$900',
        'следующее_повышение': '01.11.2023'
    }
}

# Словарь для хранения имени пользователя, его токена, времени действия токена, срока его службы
token_generated = {}

# Функция генерирования токена
def token_generator(username):
    token = secrets.token_urlsafe(16)
    active_time = timedelta(minutes=20)  # Устанавливаем активное время токена в 20 минут
    expires = datetime.now() + active_time
    token_generated[username] = {
        'токен': token,
        'активное_время': active_time,
        'истекает': expires
    }


# Эндпоинт начальной страницы
@app.get('/')
def welcome():
    return {'Приветствие': 'Добро пожаловать в REST-сервис просмотра текущей зарплаты и даты следующего повышения.'}


# Эндпоинт страницы логирования в систему
@app.post("/login")
def login(username: str, password: str):
    for u, p in db.items():
        if u.lower() == username.lower() and p['пароль'] == password:
            token_generator(u)  # Генерируем токен для пользователя
            active_time_minutes = int(
                token_generated[u]['активное_время'].total_seconds() / 60)  # Получаем время действия токена в минутах
            return {
                'Ваш токен': f"{token_generated[u]['токен']}",
                'Время действиия токена': f"{active_time_minutes} минут"
            }

    raise HTTPException(status_code=401, detail='Некорретные данные пользователя')


# Эндпоинт ввода токена и получения персональной информации
@app.post('/salary')
def salary(token: str):
    for username, data in token_generated.items():
        if token == data['токен'] and datetime.now() < data['истекает']:
            user = db.get(username)
            return {
                'пользователь': username,
                'имя': user['имя'],
                'текущая_зарплата': user['текущая_зарплата'],
                'следующее_повышение': user['следующее_повышение']
            }
    raise HTTPException(status_code=401, detail='Неверный токен либо время его действия истекло')

